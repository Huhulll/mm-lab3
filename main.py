from mcg import mcg
from normal import normal_generator
from lognormal import lognormal_generator
from exponential import exponential_generator
from laplace import laplace_generator
from weibull import weibull_generator
from criteria import pearson_criterion, kolmogorov_criterion
from distributions import normal_distribution, lognormal_distribution, exponential_distribution, laplace_distribution, \
    weibull_distribution
from utils import expectation, dispersion


def test_criterion(criterion, params):
    try:
        return "PASS" if criterion(*params) else "FAIL"
    except (ArithmeticError, ValueError):
        return "ERROR"


if __name__ == "__main__":
    mcg_params = dict(modulus=2 ** 31, a=16807, seed=16807, count=1000)
    distr = {"Normal distribution":
                 (normal_generator(mu=1, sigma2=9,
                                   **mcg_params), lambda x: normal_distribution(x, mu=0, sigma2=1)),
             "Log-normal distribution":
                 (lognormal_generator(mu=1, sigma2=9,
                                      **mcg_params), lambda x: lognormal_distribution(x, mu=1, sigma2=9)),
             "Exponential distribution":
                 (exponential_generator(2,
                                        mcg(**mcg_params)), lambda x: exponential_distribution(x, 2)),
             "Laplace distribution":
                 (laplace_generator(0.5,
                                    mcg(**mcg_params)), lambda x: laplace_distribution(x, 0.5)),
             "Weibull distribution":
                 (weibull_generator(1, 0.5,
                                    mcg(**mcg_params)), lambda x: weibull_distribution(x, 1, 0.5))}
    for name, (generator, func) in distr.items():
        print(name)
        seq = list(generator)
        print(seq)
        if func:
            print("Pearson criterion:", test_criterion(pearson_criterion, (seq, 25, func)))
            print("Kolmogorov criterion:", test_criterion(kolmogorov_criterion, (seq, 25, func)))
        m = expectation(seq)
        print("Mean", m)
        print("Disp", dispersion(seq, m))
        print()
