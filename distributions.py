import math


def normal_distribution(x, mu, sigma2):
    sigma = math.sqrt(sigma2)
    return math.exp(-0.5 * ((x - mu) / sigma) ** 2) / (sigma * math.sqrt(2 * math.pi))


def lognormal_distribution(x, mu, sigma2):
    return math.exp(normal_distribution(x, mu, sigma2))


def exponential_distribution(x, a):
    return a*math.exp(-a*x)


def laplace_distribution(x, a):
    return math.copysign(1, -x) * math.exp(-a * x) / 2


def weibull_distribution(x, a, b):
    return 1 - math.exp(-a * x ** b)
